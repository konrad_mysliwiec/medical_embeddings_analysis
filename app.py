import streamlit as st
import settings
from bert_embedding import vectorise_semmed_data
from drawing_utils import plot_2d_embeddings_plotly
from semmed_utils import get_semmed_ambiguous_phrases


def main():
    st.title("Contextualised Embedding Visualisation")
    add_selectbox = st.sidebar.selectbox(
        "Select option",
        [
            "Visualise ambiguous embeddings",
            "Take a list of ambiguous texts"
        ]
    )
    if add_selectbox == "Visualise ambiguous embeddings":
        ambiguous_embeddings()
    elif add_selectbox == "Take a list of ambiguous texts":
        ambiguous_texts_list()


def ambiguous_embeddings():
    model_name = st.text_input(
        "Model name",
        settings.DEFAULT_TRANSFORMER
    )
    entity = st.text_input(
        "Entity text",
        "ER"
    )
    embeddings, dataset = vectorise_semmed_data(
        entity,
        model_name
    )
    plot_button = st.button("Plot embeddings")
    if plot_button:
        fig = plot_2d_embeddings_plotly(
            dataset=dataset,
            embeddings=embeddings,
            title=model_name,
            display=False
        )
        st.plotly_chart(fig)


def ambiguous_texts_list():
    ambiguous_phrases = get_semmed_ambiguous_phrases()
    phrases_list = st.selectbox("Ambiguous phrases", ambiguous_phrases)


if __name__ == "__main__":
    main()
