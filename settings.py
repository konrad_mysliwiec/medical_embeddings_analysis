import os.path as osp


# DATA FILEPATHS
SEMMED_DATA_FILEPATH = osp.join(
    'data',
    'semmed'
)
PREDICATION_AUX_FILEPATH = osp.join(
    SEMMED_DATA_FILEPATH,
    'PREDICATION_AUX.csv'
)
PREDICATION_FILEPATH = osp.join(
    SEMMED_DATA_FILEPATH,
    'PREDICATION.csv'
)
SENTENCE_FILEPATH = osp.join(
    SEMMED_DATA_FILEPATH,
    'SENTENCE.csv'
)
SEMMED_MERGED_DATA_FILEPATH = osp.join(
    SEMMED_DATA_FILEPATH,
    'semmed_merged.csv'
)

# TRANSFORMERS
DEFAULT_TRANSFORMER = \
    "microsoft/BiomedNLP-PubMedBERT-base-uncased-abstract-fulltext"
