from semmed_utils import get_semmed_data_for_text
import pandas as pd
from transformers_utils import get_word_vector, TransformerHelper

import umap
from sklearn.preprocessing import StandardScaler


def get_processed_semmed_data(entity: str, model_name: str):
    transformer_helper = TransformerHelper(model_name)

    subj_df = get_semmed_data_for_text(entity, "SUBJECT_TEXT")
    subj_df = subj_df[
        [
            'SUBJECT_TEXT', 'SUBJECT_NAME', 'SUBJECT_CUI',
            'SUBJECT_SEMTYPE', 'SECTION_HEADER'
        ]
    ]
    subj_vecs = [
        get_word_vector(sent, entity, transformer_helper)
        for sent in subj_df['SECTION_HEADER'].to_numpy()
    ]
    subj_df['vecs'] = subj_vecs
    subj_df = subj_df[~subj_df['vecs'].isna()]

    obj_df = get_semmed_data_for_text(entity, "OBJECT_TEXT")
    obj_df = obj_df[
        [
            'OBJECT_TEXT', 'OBJECT_NAME', 'OBJECT_CUI',
            'OBJECT_SEMTYPE', 'SECTION_HEADER'
        ]
    ]
    obj_vecs = [
        get_word_vector(sent, entity, transformer_helper)
        for sent in obj_df['SECTION_HEADER'].to_numpy()
    ]
    obj_df['vecs'] = obj_vecs
    obj_df = obj_df[~obj_df['vecs'].isna()]

    subj_df.columns = [
        column.replace("SUBJECT", "ENTITY")
        for column in subj_df.columns
    ]
    obj_df.columns = [
        column.replace("OBJECT", "ENTITY")
        for column in obj_df.columns
    ]

    return pd.concat([subj_df, obj_df])


def vectorise_semmed_data(entity: str, model_name: str):
    semmed_data = get_processed_semmed_data(entity, model_name)
    reducer = umap.UMAP()
    vecs = [vec.tolist() for vec in semmed_data['vecs']]
    vecs_scaled = StandardScaler().fit_transform(vecs)
    embedding = reducer.fit_transform(vecs_scaled)
    return embedding, semmed_data
