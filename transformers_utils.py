import numpy as np
import torch
from transformers import AutoTokenizer, AutoModel
# from transformers.models.bert.modeling_bert import BertModel
# from transformers.models.bert.tokenization_bert_fast import BertTokenizerFast


class TransformerHelper:
    def __init__(self, model_name: str) -> None:
        self.model = AutoModel.from_pretrained(
            model_name
        )
        self.model.config.output_hidden_states = True
        self.tokenizer = AutoTokenizer.from_pretrained(
            model_name
        )

    def get_model(self):
        return self.model

    def get_tokenizer(self):
        return self.tokenizer


def get_word_idx(sent: str, word: str):
    sent = clean_sentence(sent)
    try:
        word_idx = sent.split(" ").index(word)
        return word_idx
    except Exception as e:
        return None


def get_hidden_states(encoded, token_ids_word, model):
    """Push input IDs through model.
       Select only those subword token outputs
       that belong to our word of interest and average them.
       """
    with torch.no_grad():
        output = model(**encoded)

    # Get all hidden states
    states = output.hidden_states
    # Stack and sum all requested layers
    output = states[0].squeeze()
    # Only select the tokens that constitute the requested word
    word_tokens_output = output[token_ids_word]

    return word_tokens_output.mean(dim=0)


def get_word_vector(
    sent: str,
    word: str,
    transformer: TransformerHelper = None
):
    """Get a word vector by first tokenizing the input sentence,
    getting all token idxs that make up the word of interest,
    and then `get_hidden_states`.
    """
    if transformer is None:
        transformer = TransformerHelper(
            "microsoft/BiomedNLP-PubMedBERT-base-uncased-abstract-fulltext",
        )
        tokenizer = transformer.get_tokenizer()
        model = transformer.get_model()
    else:
        tokenizer = transformer.get_tokenizer()
        model = transformer.get_model()
    encoded = tokenizer.encode_plus(sent, return_tensors="pt")
    # get all token idxs that belong to the word of interest
    idx = get_word_idx(sent, word)
    if not idx:
        return None
    token_ids_word = np.where(np.array(encoded.word_ids()) == idx)

    return get_hidden_states(encoded, token_ids_word, model)


def clean_sentence(sent: str):
    return sent.replace(".", " .").replace(", ", " , ")
