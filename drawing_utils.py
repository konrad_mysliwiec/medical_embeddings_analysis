import matplotlib.pyplot as plt
import plotly.express as px
from numpy import ndarray
from pandas import DataFrame


def draw_2d_embeddings(
    embeddings: ndarray,
    labels: ndarray,
    display: bool = True,
    cmap: str = 'Spectral',
    title: str = None,
    save_dir: str = None
):
    labels_mapper = {
        label: i
        for i, label in enumerate(set(labels))
    }
    classes = [
        labels_mapper[label]
        for label in labels
    ]
    fig, ax = plt.subplots()
    scatter = ax.scatter(
        embeddings[:, 0],
        embeddings[:, 1],
        c=classes,
        cmap=cmap
        )
    legend = ax.legend(
        scatter.legend_elements()[0],
        labels_mapper.keys()
    )
    ax.add_artist(legend)

    plt.gca().set_aspect('equal', 'datalim')
    if title:
        plt.title(title, fontsize=24)
    if save_dir:
        plt.savefig(save_dir)
    if display:
        plt.show()


def plot_2d_embeddings_plotly(
    dataset: DataFrame,
    embeddings: ndarray,
    emb_x_col: str = 'emb_x',
    emb_y_col: str = 'emb_y',
    color_col: str = 'ENTITY_NAME',
    hover_name_col: str = 'SECTION_HEADER',
    title: str = None,
    display: bool = True
):
    if embeddings is not None:
        dataset[emb_x_col] = embeddings[:, 0]
        dataset[emb_y_col] = embeddings[:, 1]
    dataset[hover_name_col] = dataset[hover_name_col]. \
        str.wrap(30)
    dataset[hover_name_col] = dataset[hover_name_col]. \
        apply(lambda x: x.replace('\n', '<br>'))
    fig = px.scatter(
        dataset,
        x=emb_x_col, y=emb_y_col,
        color=color_col,
        hover_name=hover_name_col,
        title=title
        )
    if display:
        fig.show()
    else:
        return fig
