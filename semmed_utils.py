import pandas as pd
from numpy import ndarray
import settings
import os.path as osp


def get_semmed_subset(
    sentence_nrows: int = 9000000,
    pred_nrows: int = 200000,
    pred_aux_nrows: int = 200000
):
    sent_columns = [
        'SENTENCE_ID', 'PMID', 'TYPE', 'NUMBER', 'SENTENCE',
        'SECTION_HEADER', 'NORMALIZED_SECTION_HEADER'
        ]
    sent = pd.read_csv(
        settings.SENTENCE_FILEPATH,
        nrows=sentence_nrows,
        usecols=list(range(7)),
        header=None
    )
    sent.columns = sent_columns

    pred_columns = [
        'PREDICATION_ID', 'SENTENCE_ID', 'PMID', 'PREDICATE',
        'SUBJECT_CUI', 'SUBJECT_NAME', 'SUBJECT_SEMTYPE', 'SUBJECT_NOVELTY',
        'OBJECT_CUI', 'OBJECT_NAME', 'OBJECT_SEMTYPE', 'OBJECT_NOVELTY'
        ]
    pred = pd.read_csv(
        settings.PREDICATION_FILEPATH,
        nrows=pred_nrows,
        header=None
    )
    pred.drop(columns=[12, 13, 14], inplace=True)
    pred.columns = pred_columns

    pred_aux = pd.read_csv(
        settings.PREDICATION_AUX_FILEPATH,
        nrows=pred_aux_nrows,
        header=None
    )
    pred_aux_columns = [
        'PREDICATION_AUX_ID', 'PREDICATION_ID', 'SUBJECT_TEXT',
        'SUBJECT_DIST', 'SUBJECT_MAXDIST', 'SUBJECT_START_INDEX',
        'SUBJECT_END_INDEX', 'SUBJECT_SCORE', 'INDICATOR_TYPE',
        'PREDICATE_START_INDEX', 'PREDICATE_END_INDEX', 'OBJECT_TEXT',
        'OBJECT_DIST', 'OBJECT_MAXDIST', 'OBJECT_START_INDEX',
        'OBJECT_END_INDEX', 'OBJECT_SCORE', 'CURR_TIMESTAMP'
        ]
    pred_aux.columns = pred_aux_columns

    pred_merged = pred.merge(
        pred_aux,
        left_on=['PREDICATION_ID'],
        right_on=['PREDICATION_ID']
    )
    pred_merged = pred_merged.merge(
        sent,
        left_on=['SENTENCE_ID'],
        right_on=['SENTENCE_ID']
    )
    pred_merged.to_csv(
        settings.SEMMED_MERGED_DATA_FILEPATH,
        sep='|',
        index=False
    )


def get_semmed_data_for_text(text: str, column_name: str) -> pd.DataFrame:
    if not osp.isfile(settings.SEMMED_MERGED_DATA_FILEPATH):
        get_semmed_subset()
    semmed_df = pd.read_csv(settings.SEMMED_MERGED_DATA_FILEPATH, sep='|')
    semmed_df = semmed_df[semmed_df[column_name] == text]
    return semmed_df


def get_semmed_ambiguous_phrases() -> ndarray:
    df = pd.read_csv(
        settings.SEMMED_MERGED_DATA_FILEPATH,
        sep='|'
    )
    df = df[['SUBJECT_TEXT', 'SUBJECT_NAME']] \
        .groupby("SUBJECT_TEXT") \
        .agg(lambda x: len(set(x)) > 1)

    return df[df['SUBJECT_NAME']].index.to_numpy()


if __name__ == '__main__':
    get_semmed_subset()
